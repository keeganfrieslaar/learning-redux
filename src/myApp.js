import React from "react";
import InputText from "./components/InputText";
import { connect } from "react-redux";
import "./index.css";

class MyApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onTextChange = () => {
    const { text } = this.props;
  };

  render() {
    const { text, textSize, textColor } = this.props;
    return (
      <div className="myApp" style={{ textlign: "center" }}>
        <h1 style={{ fontSize: textSize }}>{text}</h1>
        <InputText />
      </div>
    );
  }
}

export default connect(
  state => ({
    ...state.App,
    text: state.TextProps.text,
    textSize: state.TextProps.textSize,
    textColor: state.TextProps.textColor
  }),
  {}
)(MyApp);
