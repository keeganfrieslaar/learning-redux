import React from "react";
import { connect } from "react-redux";
import textActions from "../redux/actions";

const { textChange, textSizeChange } = textActions;

class InputText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onTextChange = e => {
    const { text, textChange, textSize } = this.props;
    const value = e.target.value;
    console.log("value", value);
    textChange(value);
  };

  changeSizeBig = () => {
    const { textSizeChange } = this.props;
    textSizeChange("20px");
  };

  changeSizeSmall = () => {
    const { textSizeChange } = this.props;
    textSizeChange("14px");
  };

  render() {
    const { text, textSize, textSizeChange } = this.props;
    return (
      <div>
        <input onChange={this.onTextChange} value={text} />
        <br />
        <button onClick={this.changeSizeBig}>Change Text Size to 20px</button>
        <br />
        <button onClick={this.changeSizeSmall}>Change Text Size to 14px</button>
        <br />
        <button onClick={this.changeColorBlue}>
          Change Text Color to Blue
        </button>
        <br />
        <button onClick={this.changeColorBlack}>
          Change Text Size to Black
        </button>
      </div>
    );
  }
}

export default connect(
  state => ({
    ...state.App,
    text: state.TextProps.text,
    textSize: state.TextProps.textSize
  }),
  { textChange, textSizeChange }
)(InputText);
